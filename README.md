#BLS dose UV
d6 <- Data_BLS3
head(d6)
anova_BLS <- aov(formula = AUDPC ~ Treatment*Day, data = d6)
summary(anova_BLS)


#Caluculde moy et sd coloration
library(dplyr)
library(ggplot2)
head(d6)
data_summary <- function(data, varname, groupnames){
  require(plyr)
  summary_func <- function(x, col){
    c(mean = mean(x[[col]], na.rm=TRUE),
      sd = sd(x[[col]], na.rm=TRUE))
  }
  data_sum<-ddply(data, groupnames, .fun=summary_func,
                  varname)
  data_sum <- rename(data_sum, c("mean" = varname))
  return(data_sum)
}
#Resume
dft <- data_summary(d6, varname = "AUDPC", 
                    groupnames=c("Treatment", "Day"))
head(dft)